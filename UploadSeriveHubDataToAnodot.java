package com.appFirst.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class UploadSeriveHubDataToAnodot extends UploadServiceHubDataToAnodot {

	public static String uploadServerDataToAnodot(String jsonString,int serverId) throws Exception {
		String aondotString= "";
		String next = "";
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		final JSONObject pagination = jsonObject.getJSONObject("pagination"); 
		next = pagination.getString("next");
		final JSONArray geodata = jsonObject.getJSONArray("data");
		JSONObject obj = new JSONObject();
		JSONArray jsonArray = getJsonArrayToUploadServerData(geodata,serverId);
		obj.put("anodot", jsonArray);
		aondotString = obj.getString("anodot");
		if(geodata.size()>0){
			sendRequestToAnodot(aondotString);
		}
		return aondotString;
	}
	
	public static JSONArray getJsonArrayToUploadServerData(final JSONArray AppfirstData,int serverId) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < AppfirstData.size(); i++) {
			final JSONObject data = AppfirstData.getJSONObject(i);
			String serverName ="";
			
			switch (serverId) {
			case 92:
				serverName= "shmun09";
				break;
		case 93:
				serverName= "tribefire";
				break;
		case 94:
				serverName= "nginx";
				break;
		case 95:
				serverName= "cloud-connector";
				break;
		case 96:
				serverName= "postgresql";
				break;
		case 97:
				serverName= "mongodb";
				break;
		case 98:
				serverName= "mfp";
				break;
		case 99:
				serverName= "oauth-service";
				break;
		case 100:
				serverName= "idm";
				break;
		case 101:
				serverName= "assets";
				break;
		case 102:
				serverName= "opendj";
				break;
		}
			
			addJsonObjectToArray("cpu", "server="+serverName+".MetricType=cpu.what=CPUutilization", jsonArray, data);
			addJsonObjectToArray("thread_num","server="+serverName+".MetricType=thread.what=ThreadNum", jsonArray, data);
			addJsonObjectToArray("memory","server="+serverName+".MetricType=memory.what=memory", jsonArray, data);
			addJsonObjectToArray("process_num","server="+serverName+".MetricType=process_num.what=process_num", jsonArray, data);
			addJsonObjectToArray("page_faults","server="+serverName+".MetricType=page_faults.what=page_faults", jsonArray, data);
			addJsonObjectToArray("disk_busy_max","server="+serverName+".MetricType=disk_busy_max.what=disk_busy_max", jsonArray, data);
			addJsonObjectToArray("diskp_max","server="+serverName+".MetricType=diskusage_max.what=diskusage_max", jsonArray, data);
			addJsonObjectToArray("avg_response_time","server="+serverName+".MetricType=avg_response_time.what=avg_response_time", jsonArray, data);
			addJsonObjectToArray("socket_write","server="+serverName+".MetricType=InboundNetworkTraffic.what=InboundNetworkTraffic", jsonArray, data);
			addJsonObjectToArray("srw_max","server="+serverName+".MetricType=OutboundNetworkTraffic.what=OutboundNetworkTraffic", jsonArray, data);
			
			
		}
		return jsonArray;
	}
	
	public static void addJsonObjectToArray(String metricType, String name, JSONArray jsonArray,
			final JSONObject data) {
		JSONObject tempJsonObject;
		tempJsonObject = new JSONObject();
		tempJsonObject.put("name", name);
		tempJsonObject.put("timestamp", data.getString("time"));
		try{
			tempJsonObject.put("value", data.getString(metricType));
		}catch(Exception e){
			//e.printStackTrace();
			tempJsonObject.put("value", "0");
		}
		jsonArray.add(tempJsonObject);
	}

	
	
	
}
