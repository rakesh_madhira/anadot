package com.appFirst.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class UploadAnodotData {
	public static void main(String[] args) throws Exception {
		
		
		int sleepTime = 60000;
		String ipAddress ="10.1.105.67";
		//ipAddress = "frontend.konicaminoltabusiness.comcastbiz.net";
		int serverIds [] = {52,72,73,74,75,76,77,78,79,80,81,82,25,
				63,17,18,19,20,21,22,70,23,30,31,24,58,59,
				60,61,62,29,27,28,37,55,56,57,40,38,39,47,
				41,43,48,49,50,51,53,54,36,34,42,71,45,108,109,110,111,112} ;
		int i =0;
		while(true){
			try{
				long epoch = System.currentTimeMillis()/1000;
				//System.out.println("Epoch ::::::::::: "+epoch);
				String startTime = String.valueOf(epoch -180);
				String endTime = String.valueOf(epoch -180);
				//System.out.println(" startTime :::: "+startTime+" endTime ::: "+endTime);
				uploadAnodotServerData(ipAddress, startTime, endTime,serverIds);
				
				//uploadAnodotProcessData(ipAddress, startTime, endTime,serverIds);
				Thread.sleep(sleepTime);
				
				
				/*if(i==0){
					int a[] = {0,2,3};
					System.out.println(a[100]);
				}
				i++;*/
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	public static void uploadAnodotServerData(String ipAddress,String startTime,String endTime,int[] serverIds) throws Exception{
		
		
		for (int i = 0; i < serverIds.length; i++) {
			int serverId = serverIds[i];
			//System.out.println("ServerID  ServerData ::::::::::::::::::::::::: "+serverIds[i]);
			//System.out.println("https://"+ipAddress+"/api/servers/"+serverId+"/data/?format=json&start="+startTime+"&end="+endTime);
			String jsonString = getDataFromAppFirst("https://"+ipAddress+"/api/servers/"+serverId+"/data/?format=json&start="+startTime+"&end="+endTime,"rmadhira+api@kmbs.konicaminolta.us","Raki@123"); 
			if(!"NoDetailsFound".equalsIgnoreCase(jsonString)){
				UploadAnodotServerData.uploadServerDataToAnodot(jsonString,serverId);
				System.out.println("ServerID  ::::::::::::::::::::::::: "+serverIds[i] +" :: Uploded ::: Time :::: "+startTime);
			}
		}
	}
	public static String   getDataFromAppFirst(String GET_URL,String userName,String password) throws Exception {

		String jsonString ="";
		
		disableSSLVerification();
		//System.out.println(":::::::::::::::::::::::::::::::::::: After  Certificate Verification ::::::::::::::::::::::::::::::::::::::::::::::::: ");
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        String encodedBytes = Base64.encodeBase64String((userName+":"+password).getBytes());
        con.setRequestMethod("GET");
        con.setRequestProperty("Authorization", "Basic "+encodedBytes);
		con.setRequestProperty("Content-Type", "application/json");
		//System.out.println("Authorization ::::: Basic "+encodedBytes);
        int responseCode = con.getResponseCode();
        //System.out.println("APPFIRST Response Code :: " + responseCode);
        if (responseCode == HttpsURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            // print result
            //System.out.println(response.toString());
            jsonString = response.toString();
        } else {
            System.out.println("GET request not worked :: "+GET_URL);
            jsonString = "NoDetailsFound";
        }
        
        return jsonString;
	}
	
	public static void disableSSLVerification() {

        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };      
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);           
    }
	
public static void sendRequestToAnodot(String data)throws Exception{
		
		String GET_URL ="https://api.anodot.com/api/v1/metrics?token=56723b0ac97bc37f4a0ece0c";
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);
		String str =  data;
		//str =  "[{'name':'metric.example.1','timestamp':'1450830417','value':100,'tags':{'target_type':'counter'}},{'name':'metric.example.1','timestamp':'1450830441','value':200,'tags':{'target_type':'counter'}}]";
    	byte[] outputInBytes = str.getBytes("UTF-8");
    	OutputStream os = con.getOutputStream();
    	os.write( outputInBytes );    
    	os.close();
		int responseCode = con.getResponseCode();
        //System.out.println("Anodot Response Code :: " + responseCode);
	}
}
