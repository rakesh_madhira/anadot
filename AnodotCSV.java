package com.appFirst.util;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;


import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class AnodotCSV {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
		System.setProperty("jsse.enableSNIExtension", "false");
		AnodotCSV http = new AnodotCSV();
		String startTime = "1446015600"; 
		String endTime ="1451931468"; 
		
		//stratTime = "1452109338"; 
		endTime ="1452111000"; 
		/*String file = "C:\\Users\\rakesh\\Desktop\\TestFile.csv";
		FileWriter fw = new FileWriter(file);
		StringBuilder line = new StringBuilder("Message Source,Message Body\n");
		fw.write(line.toString());
		line = new StringBuilder("test ,Message Body Test");
		
		fw.write(line.toString());
        fw.close();*/
		
		int serverId= 38;
		/*long epoch = System.currentTimeMillis()/1000;
		System.out.println("epoch :::::::::::::::::::::::: "+epoch);
		endTime = String.valueOf(epoch);*/
		//endTime =1452198480;
		//int serverIds [] = {42,36,37,38,47,48,49,50,51,53,54,55,56,57,40,39,41,43} ;
		int serverIds [] = {47,48,49,50,51,53,54,55,56,57,40,39,41,43} ;
		//int serverIds [] = {serverId} ;
		int recordCount = 1;
		
		for (int i = 0; i < serverIds.length; i++) {
			serverId = serverIds[i];
			System.out.println("ServerID ::::::::::::::::::::::::: "+serverId+"Server Name :: "+getServerName(serverId));
			String file = "C:\\Users\\rakesh\\Desktop\\anodot\\AnodotDataFile_"+getServerName(serverId)+"_"+startTime+"_"+endTime+".csv";
			FileWriter fw = new FileWriter(file);
			String line = "TimeStamp,Value,MetricName\n";
			fw.write(line);
			//String jsonString = http.getServerData("https://10.1.105.67/api/servers/"+serverId+"/data/?format=json&start="+stratTime+"&end="+endTime,"rmadhira+api@kmbs.konicaminolta.us","Raki@123");
			String jsonString = http.getServerData("https://10.1.105.67/api/servers/"+serverId+"/data/?format=json&start="+startTime+"&end="+endTime,"rmadhira+api@kmbs.konicaminolta.us","Raki@123");
			//String jsonString = http.getServerData("https://frontend.konicaminoltabusiness.comcastbiz.net/api/servers/"+serverId+"/data/?format=json&start="+stratTime+"&end="+endTime,"rmadhira+api@kmbs.konicaminolta.us","Raki@123");
			recordCount = wirteToCsvFle(jsonString,serverId,startTime,endTime,fw,recordCount);
			fw.close();
			System.out.println("recordCount ::::::::::::::::::: "+recordCount);
		}
		
		System.out.println("recordCount ::::::::::::::::::: "+recordCount);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static int wirteToCsvFle(String jsonString,int serverId,String startTime,String endTime,FileWriter fw, int recordCount) throws Exception {
		String next = "";
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		final JSONObject pagination = jsonObject.getJSONObject("pagination"); 
		next = pagination.getString("next");
		JSONArray geodata = jsonObject.getJSONArray("data");
		//System.out.println("Data Array Size ::::::: "+geodata.size());
		recordCount = writeToFile(serverId, recordCount, geodata, fw);
		//System.out.println("next Api URL ::::::::::::: "+next);
		
		if(next!=null && !"null".equalsIgnoreCase(next)){
			
			while(next!=null && !"null".equalsIgnoreCase(next)){
				String tempString  =  new AnodotCSV().getServerData(next,"rmadhira+api@kmbs.konicaminolta.us","Raki@123");	
				JSONObject nextObject = JSONObject.fromObject(tempString);
				final JSONObject nextPagination = nextObject.getJSONObject("pagination"); 
				next = nextPagination.getString("next");
				geodata = nextObject.getJSONArray("data");
				recordCount = writeToFile(serverId, recordCount, geodata, fw);
				
				System.out.println("next Pagination  ::::::: "+next);
			}
		}
		return recordCount;
	}

	public static int writeToFile(int serverId, int recordCount, JSONArray geodata, FileWriter fw) {
		for (int i = 0; i < geodata.size(); i++) {
			final JSONObject data = geodata.getJSONObject(i);
			try{
				fw.write(getDataString("cpu", "server="+getServerName(serverId)+".MetricType=cpu.what=CPUutilization", data));
				fw.write(getDataString("thread_num","server="+getServerName(serverId)+".MetricType=thread.what=ThreadNum",  data));
				fw.write(getDataString("memory","server="+getServerName(serverId)+".MetricType=memory.what=memory",  data));
				fw.write(getDataString("process_num","server="+getServerName(serverId)+".MetricType=process_num.what=process_num",  data));
				fw.write(getDataString("page_faults","server="+getServerName(serverId)+".MetricType=page_faults.what=page_faults",  data));
				fw.write(getDataString("disk_busy_max","server="+getServerName(serverId)+".MetricType=disk_busy_max.what=disk_busy_max",  data));
				fw.write(getDataString("diskp_max","server="+getServerName(serverId)+".MetricType=diskusage_max.what=diskusage_max",  data));
				fw.write(getDataString("avg_response_time","server="+getServerName(serverId)+".MetricType=avg_response_time.what=avg_response_time",  data));
				fw.write(getDataString("socket_write","server="+getServerName(serverId)+".MetricType=InboundNetworkTraffic.what=InboundNetworkTraffic",  data));
				fw.write(getDataString("srw_max","server="+getServerName(serverId)+".MetricType=OutboundNetworkTraffic.what=OutboundNetworkTraffic",  data));
				recordCount += 10;
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return recordCount;
	}
	
	private static String getDataString(String metricType,String metricName,final JSONObject data){
		String dataString="";
		
		try {
			dataString = data.getString("time")+","+data.getString(metricType)+","+metricName+"\n";
		} catch (Exception e) {
			dataString = data.getString("time")+",0,"+metricName+"\n";
			// TODO: handle exception
		}
		return dataString;
		
	}
	
	private String  getServerData(String GET_URL,String userName,String password) throws Exception {

		String jsonString ="";
		
		disableSSLVerification();
		//System.out.println(":::::::::::::::::::::::::::::::::::: After  Certificate Verification ::::::::::::::::::::::::::::::::::::::::::::::::: ");
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        String encodedBytes = Base64.encodeBase64String((userName+":"+password).getBytes());
        con.setRequestMethod("GET");
        con.setRequestProperty("Authorization", "Basic "+encodedBytes);
		con.setRequestProperty("Content-Type", "application/json");
		//System.out.println("Authorization ::::: Basic "+encodedBytes);
        int responseCode = con.getResponseCode();
        System.out.println("APPFIRST Response Code :: " + responseCode);
        if (responseCode == HttpsURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            // print result
            //System.out.println(response.toString());
            jsonString = response.toString();
        } else {
            System.out.println("GET request not worked");
        }
        
        return jsonString;
	}
	
	public static void disableSSLVerification() {

        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };      
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);           
    }
	
	private static String getServerName(int serverId){
		String serverName ="";
		try {
			
			switch (serverId) {
				case 42:
						serverName ="vcareeif";
					break;
				case 36:
					serverName ="vcaredb";
				break;
				
				case 37:
					serverName ="vcarecom1";
				break;
				case 38:
					serverName ="vcarecom2";
				break;
				case 47:
					serverName ="vcarecom3";
				break;
				case 48:
					serverName ="vcarecom4";
				break;
				case 49:
					serverName ="vcarecom5";
				break;
				case 50:
					serverName ="vcarecom6";
				break;
				case 51:
					serverName ="vcarecom7";
				break;
				case 53:
					serverName ="vcarecom8";
				break;
				case 54:
					serverName ="vcarecom9";
				break;
				case 55:
					serverName ="vcarecom10";
				break;
				case 56:
					serverName ="vcarecom11";
				break;
				case 57:
					serverName ="vcarecom12";
				break;
				case 40:
					serverName ="vcarecom15";
				break;
				case 39:
					serverName ="vcarecom20";
				break;
				case 41:
					serverName ="vcarecom30";
				break;
				case 43:
					serverName ="vcarecom38";
				break;
			
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return serverName;
		
	}
}
