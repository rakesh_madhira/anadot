package com.appFirst.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.ArrayUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class UploadAnodotProcessDataSAP  extends Thread{
	public static void main(String[] args) throws Exception {
		
		//ipAddress = "frontend.konicaminoltabusiness.comcastbiz.net";
		//int serverIds [] = {73,74,75,76,77,78,79,80,81,82} ;
		UploadAnodotProcessDataSAP server73 = new UploadAnodotProcessDataSAP();
		server73.setName("73");
		UploadAnodotProcessDataSAP server74 = new UploadAnodotProcessDataSAP();
		server74.setName("74");
		UploadAnodotProcessDataSAP server75 = new UploadAnodotProcessDataSAP();
		server75.setName("75");
		UploadAnodotProcessDataSAP server76 = new UploadAnodotProcessDataSAP();
		server76.setName("76");
		UploadAnodotProcessDataSAP server77 = new UploadAnodotProcessDataSAP();
		server77.setName("77");
		UploadAnodotProcessDataSAP server78 = new UploadAnodotProcessDataSAP();
		server78.setName("78");
		UploadAnodotProcessDataSAP server79 = new UploadAnodotProcessDataSAP();
		server79.setName("79");
		UploadAnodotProcessDataSAP server80 = new UploadAnodotProcessDataSAP();
		server80.setName("80");
		UploadAnodotProcessDataSAP server81 = new UploadAnodotProcessDataSAP();
		server81.setName("81");
		UploadAnodotProcessDataSAP server82 = new UploadAnodotProcessDataSAP();
		server82.setName("82");
		
		UploadAnodotProcessDataSAP server17 = new UploadAnodotProcessDataSAP();
		server17.setName("17");
		UploadAnodotProcessDataSAP server18 = new UploadAnodotProcessDataSAP();
		server18.setName("18");
		UploadAnodotProcessDataSAP server19 = new UploadAnodotProcessDataSAP();
		server19.setName("19");
		UploadAnodotProcessDataSAP server20 = new UploadAnodotProcessDataSAP();
		server20.setName("20");
		UploadAnodotProcessDataSAP server21 = new UploadAnodotProcessDataSAP();
		server21.setName("21");
		UploadAnodotProcessDataSAP server22 = new UploadAnodotProcessDataSAP();
		server22.setName("22");
		
		server73.start();
		server74.start();
		server75.start();
		server76.start();
		server77.start();
		server78.start();
		server79.start();
		server80.start();
		server81.start();
		server82.start();
		
		server17.start();
		server18.start();
		server19.start();
		server20.start();
		server21.start();
		server22.start();
	}
	
	public void runThread(String ipAddress,int[] serverIds){
		
		try{
			long epoch = System.currentTimeMillis()/1000;
			String startTime = String.valueOf(epoch -180);
			String endTime = String.valueOf(epoch -180);
			uploadAnodotProcessData(ipAddress, startTime, endTime,serverIds);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void run() {
		try {
			while(true){
				String ipAddress = "10.1.105.67";
				// System.out.println("Runnable
				// Thread:"+Thread.currentThread().getName());
				int serverId = Integer.valueOf(Thread.currentThread().getName());
				runThread(ipAddress, new int[] { serverId });
				Thread.sleep(60000);
			}
		} catch (InterruptedException e) {

			System.out.println(e.getMessage());

		}

	}
	
	public static void uploadAnodotProcessData(String ipAddress,String startTime,String endTime,int[] serverIds) throws Exception{
		String userName ="rmadhira+api@kmbs.konicaminolta.us";
		String password ="Raki@123";
		for (int i = 0; i < serverIds.length; i++) {
			int serverId = serverIds[i];
			String jsonString = getDataFromAppFirst("https://"+ipAddress+"/api/servers/"+serverId+"/processes/?format=json&start="+startTime+"&end="+startTime,userName,password);
			if(!"NoDetailsFound".equalsIgnoreCase(jsonString)){
				uploadProcessData(serverId,jsonString,startTime,userName,password,ipAddress);
				System.out.println("ServerID  ProcessData::::::::::::::::::::::::: "+serverIds[i] +" :: Uploded ::: Time :::: "+startTime);
			}
		}
	}
	
	public static void uploadProcessData(int serverId,String ProcessIdData, String time,String userName, String password,String ipAddress) throws Exception{
		
		String processList[]= null;
		processList = new String[]{ "disp+work","gwrd ","icman","igsmux","igspw","igswd","saphostc",
				"sapstartsrv","sapstartsrv","STSchedEx","VGAuthService","vmtoolsd",
				"FrameworkService","mcshield","mctelsvc","mfeann","naPrdMgr","VsTskMgr",
				"cms","fileserver","fileserver","java","ConnectionServer32",
				"ConnectionServer","EventServer","svnserve","sia"};

		JSONObject jsonObject = JSONObject.fromObject(ProcessIdData);
		JSONArray pdata = jsonObject.getJSONArray("data");
		for (int i = 0; i < pdata.size(); i++) {
			final JSONObject processIdJsonObj = pdata.getJSONObject(i);
			String processName= processIdJsonObj.getString("name");
			if(processName.indexOf(".")!= -1){
				processName = processName.substring(0, processName.indexOf("."));
			}
			//System.out.println("args ::::::::::::::::: "+processIdJsonObj.getString("args"));
			String tempProcessName =""; 
			if("disp+work".equalsIgnoreCase(processName)){
				tempProcessName= processIdJsonObj.getString("args");
				if(tempProcessName.indexOf("wp_id") != -1){
					tempProcessName = tempProcessName.substring(tempProcessName.indexOf("wp_id"), tempProcessName.length());
					tempProcessName =tempProcessName.replace("=", "");
					//System.out.println("tempProcessName :::::::::::::::::::::"+tempProcessName);	
				}else{
					tempProcessName="wp_id0"; 
				}
			}else if("igspw".equalsIgnoreCase(processName) || "fileserver".equalsIgnoreCase(processName)){
				tempProcessName = processIdJsonObj.getString("pid");
			}
			
			if (ArrayUtils.contains(processList, processName)) {
				
				if("mcshield".equalsIgnoreCase(processName)){
					processName = "McAfee_mschield";
				}
				if("mctelsvc".equalsIgnoreCase(processName)){
					processName = "McAfee_mctelsvc";
				}
				if("mfeann".equalsIgnoreCase(processName)){
					processName = "McAfee_mfeann";
				}
				if("naPrdMgr".equalsIgnoreCase(processName)){
					processName = "McAfee_naPrdMgr";
				}
				if("VsTskMgr".equalsIgnoreCase(processName)){
					processName = "McAfee_VsTskMgr";
				}
				
				//System.out.println("Processname ::: "+processName+" id :: "+tempProcessName+" \t\tUid ::: "+processIdJsonObj.getString("uid")+"  Pid ::: "+processIdJsonObj.getString("pid")+" Ppid :::: "+processIdJsonObj.getString("ppid") );
				
				String processDataJsonString = getDataFromAppFirst("https://" + ipAddress + "/api/processes/"
						+ processIdJsonObj.getString("uid") + "/data/?format=json&start=" + time + "&end=" + time,
						userName, password);
				jsonObject = JSONObject.fromObject(processDataJsonString);
				final JSONArray geodata = jsonObject.getJSONArray("data");
				JSONObject obj = new JSONObject();
				//System.out.println("processName+tempProcessName ::: "+processName+tempProcessName);
				JSONArray jsonArray = getJsonArrayToUploadServerData(geodata, serverId,processName+tempProcessName);
				obj.put("anodot", jsonArray);
				String aondotString = obj.getString("anodot");
				if (geodata.size() > 0) {
					sendRequestToAnodot(aondotString);
				}
			}
		}
	}
	
	public static JSONArray getJsonArrayToUploadServerData(final JSONArray AppfirstData,int serverId,String processName) {
		JSONArray jsonArray = new JSONArray();
		processName = processName.replaceAll(" ", "");
		String serverName =getServerName(serverId);
		for (int i = 0; i < AppfirstData.size(); i++) {
			final JSONObject data = AppfirstData.getJSONObject(i);
			addJsonObjectToArray("thread_num", "server="+serverName+".ProcessName="+processName+".MetricType=thread_num.what=thread_num", jsonArray, data);
			addJsonObjectToArray("socket_num", "server="+serverName+".ProcessName="+processName+".MetricType=socket_num.what=socket_num", jsonArray, data);
			addJsonObjectToArray("page_faults", "server="+serverName+".ProcessName="+processName+".MetricType=page_faults.what=page_faults", jsonArray, data);
			addJsonObjectToArray("socket_write", "server="+serverName+".ProcessName="+processName+".MetricType=socket_write.what=socket_write", jsonArray, data);
			addJsonObjectToArray("socket_read", "server="+serverName+".ProcessName="+processName+".MetricType=socket_read.what=socket_read", jsonArray, data);
			addJsonObjectToArray("memory", "server="+serverName+".ProcessName="+processName+".MetricType=memory.what=memory", jsonArray, data);
			addJsonObjectToArray("file_read", "server="+serverName+".ProcessName="+processName+".MetricType=file_read.what=file_read", jsonArray, data);
			addJsonObjectToArray("registry_num", "server="+serverName+".ProcessName="+processName+".MetricType=registry_num.what=registry_num", jsonArray, data);
			addJsonObjectToArray("critical_log", "server="+serverName+".ProcessName="+processName+".MetricType=critical_log.what=critical_log", jsonArray, data);
			addJsonObjectToArray("file_write", "server="+serverName+".ProcessName="+processName+".MetricType=file_write.what=file_write", jsonArray, data);
			addJsonObjectToArray("flags", "server="+serverName+".ProcessName="+processName+".MetricType=flags.what=flags", jsonArray, data);
			addJsonObjectToArray("file_num", "server="+serverName+".ProcessName="+processName+".MetricType=file_num.what=file_num", jsonArray, data);
			addJsonObjectToArray("response_num", "server="+serverName+".ProcessName="+processName+".MetricType=response_num.what=response_num", jsonArray, data);
			addJsonObjectToArray("cpu", "server="+serverName+".ProcessName="+processName+".MetricType=cpu.what=CPUutilization", jsonArray, data);
		}
		return jsonArray;
	}
	
	public static void addJsonObjectToArray(String metricType, String name, JSONArray jsonArray,
			final JSONObject data) {
		JSONObject tempJsonObject;
		tempJsonObject = new JSONObject();
		tempJsonObject.put("name", name);
		tempJsonObject.put("timestamp", data.getString("time"));
		try{
			tempJsonObject.put("value", data.getString(metricType));
		}catch(Exception e){
			//e.printStackTrace();
			tempJsonObject.put("value", "0");
		}
		jsonArray.add(tempJsonObject);
	}
	
	public static String   getDataFromAppFirst(String GET_URL,String userName,String password) throws Exception {

		String jsonString ="";
		
		disableSSLVerification();
		//System.out.println(":::::::::::::::::::::::::::::::::::: After  Certificate Verification ::::::::::::::::::::::::::::::::::::::::::::::::: ");
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        String encodedBytes = Base64.encodeBase64String((userName+":"+password).getBytes());
        con.setRequestMethod("GET");
        con.setRequestProperty("Authorization", "Basic "+encodedBytes);
		con.setRequestProperty("Content-Type", "application/json");
		//System.out.println("Authorization ::::: Basic "+encodedBytes);
        int responseCode = con.getResponseCode();
        //System.out.println("APPFIRST Response Code :: " + responseCode);
        if (responseCode == HttpsURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            // print result
            //System.out.println(response.toString());
            jsonString = response.toString();
        } else {
            System.out.println("GET request not worked");
            jsonString = "NoDetailsFound";
        }
        
        return jsonString;
	}
	
	public static void disableSSLVerification() {

        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };      
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);           
    }
	
public static void sendRequestToAnodot(String data)throws Exception{
		
		String GET_URL ="https://api.anodot.com/api/v1/metrics?token=56723b0ac97bc37f4a0ece0c";
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);
		String str =  data;
		//str =  "[{'name':'metric.example.1','timestamp':'1450830417','value':100,'tags':{'target_type':'counter'}},{'name':'metric.example.1','timestamp':'1450830441','value':200,'tags':{'target_type':'counter'}}]";
    	byte[] outputInBytes = str.getBytes("UTF-8");
    	OutputStream os = con.getOutputStream();
    	os.write( outputInBytes );    
    	os.close();
		int responseCode = con.getResponseCode();
        //System.out.println("Anodot Response Code :: " + responseCode);
	}

private static String getServerName(int serverId){
	String serverName ="";
	try {
			switch (serverId) {
			case 73:
				serverName = "crmcp1a01";
				break;
			case 74:
				serverName = "crmcp1a02";
				break;
			case 75:
				serverName = "crmcp1a03";
				break;
			case 76:
				serverName = "crmcp1a04";
				break;
			case 77:
				serverName = "crmcp1a05";
				break;
			case 78:
				serverName = "crmcp1a06";
				break;
			case 79:
				serverName = "crmcp1a07";
				break;
			case 80:
				serverName = "crmcp1a08";
				break;
			case 81:
				serverName = "crmcp1a09";
				break;
			case 82:
				serverName = "crmcp1a10";
				break;
			case 17:
				serverName = "kmbop1";
				break;
			case 18:
				serverName = "kmbop2";
				break;
			case 19:
				serverName = "kmbop3";
				break;
			case 20:
				serverName = "kmbop4";
				break;
			case 21:
				serverName = "kmbop5";
				break;
			case 22:
				serverName = "kmbop6";
				break;
			}
	} catch (Exception e) {
		// TODO: handle exception
	}
	return serverName;
	
}
}
