package com.appFirst.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AccessLogExample {

	static Pattern imgePattern;
	static Matcher matcher;
	final String IPADDRESS_PATTERN = 
			"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
 
	final static String IMAGE_PATTERN ="((\\.(?i)(jpg|png|gif|bmp))$)";
	
	static{
		imgePattern = Pattern.compile(IMAGE_PATTERN);
	}
	 
	 
	 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		StringBuffer temp = new StringBuffer();
		
		//temp.append("10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] GET /assets/js/lowpro.gif HTTP/1.1 200 10469 ");
		temp.append("10.223.157.186 - - [15/Jul/2009:15:50:36 -0700] GET /assets/img/dummy/secondary-news-4.jpg HTTP/1.1 200 5766");
		//temp.append("10.223.157.186 - - [15/Jul/2009:15:50:52 -0700] \"GET /assets/img/dummy/secondary-news-2.jpg HTTP/1.1\" 304 -");
		/*temp.append("10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] GET /assets/css/reset.png HTTP/1.1 200 1014 ");
		temp.append("10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] GET /assets/css/960.Jpeg HTTP/1.1 200 6206 ");
		temp.append("10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] GET /assets/css/the-associates.css HTTP/1.1 200 15779 ");
		temp.append("10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] GET /assets/js/the-associates.JPEG HTTP/1.1 200 4492 ");
		temp.append("10.223.157.186 - - [15/Jul/2009:15:50:35 -0700] GET /assets/js/lightbox.js HTTP/1.1 200 25960 ");*/
		
		/*for (String image : temp.toString().split(" ")) {
			//System.out.println("image :::::::::::: "+image);
			if(validateImage(image)){
				System.out.println("ipAddresss ::::::::::::::::::::::: "+image);
			}
		}*/
		
		/*String in = "5766";
		
		Pattern p = Pattern.compile("\\d+");
	    Matcher m = p.matcher(in);
	    while(m.find()) {
	    	 System.out.println(m.group() + " " + m.start() + " " + m.end());
	    }*/
		
		StringBuffer value = new StringBuffer("96.7.4.14 - - [24/Apr/2011:04:20:11 -0400] \"GET /cat.jpg HTTP/1.1\" 200 12433");
		String[] fields = value.toString().split("\"");
	    if (fields.length > 1) {
	      String request = fields[1];
	      
	      /*
	       * Split the part of the line after the first double quote
	       * using the space character as the delimiter to get a file name.
	       */
	      fields = request.split(" ");
	      
	      /*
	       * Increment a counter based on the file's extension.
	       */
	      if (fields.length > 1) {
	        String fileName = fields[1].toLowerCase();
	        if (fileName.endsWith(".jpg")) {
	          System.out.println(fileName );
	        } else if (fileName.endsWith(".gif")) {
	        	System.out.println(fileName );
	        } else {
	        	System.out.println(fileName );
	        }
	      }
	    }
	    
	    	
	    
		
		
		String a = "EIF Agent Concrete Service for Script.exe";
		System.out.println(a.replaceAll(" ", ""));
		
		String[] feilds = temp.toString().split(" ");
		//System.out.println(feilds.length);
		for (int i = 1; i < feilds.length; i++) {
			//System.out.println("feilds["+i+"]  ::::::::::::::::::::::::  "+feilds[i]);
			if(validateImage(feilds[i]) && validateTime(feilds[feilds.length-1]) ){
				System.out.println("feilds[i]  ::::::::::::::::::::::::  "+feilds[i]);
				System.out.println("feilds[i]  ::::::::::::::::::::::::  "+feilds[feilds.length-1]);
			}
		}
		
	}
	
	public static boolean validateTime(String time){
		Pattern p = Pattern.compile("\\d+");
	    Matcher m = p.matcher(time);	  
	    return m.matches();	
	}
	
	public static boolean validateImage(String image){
		  //System.out.println("image ::::::::::::::::::::::: "+image);
		  if(image.indexOf(".")!=-1){
			  image = image.substring(image.indexOf("."), image.length());
			  //System.out.println("Img ::::::::::::::::::::::::::::: "+image);
			  matcher = imgePattern.matcher(image);
			  return matcher.matches();	
		  }
		 return false;
	}
	
	
}
