package com.appFirst.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.ArrayUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class UploadAnodotProcessDataVcare  extends Thread{
	public static void main(String[] args) throws Exception {
		
		
		/*int sleepTime = 60000;
		String ipAddress ="10.1.105.67";
		//ipAddress = "frontend.konicaminoltabusiness.comcastbiz.net";
		int serverIds [] = {42,36,37,38,47,48,49,50,51,53,54,55,56,57,40,39,41,43} ;
		int i =0;
		while(true){
			try{
				long epoch = System.currentTimeMillis()/1000;
				String startTime = String.valueOf(epoch -120);
				String endTime = String.valueOf(epoch -120);
				uploadAnodotProcessData(ipAddress, startTime, endTime,serverIds);
				Thread.sleep(sleepTime);
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}*/
		
			UploadAnodotProcessDataVcare server42 = new UploadAnodotProcessDataVcare();
			server42.setName("42");
			UploadAnodotProcessDataVcare server36 = new UploadAnodotProcessDataVcare();
			server36.setName("36");
			UploadAnodotProcessDataVcare server37 = new UploadAnodotProcessDataVcare();
			server37.setName("37");
			UploadAnodotProcessDataVcare server38 = new UploadAnodotProcessDataVcare();
			server38.setName("38");
			UploadAnodotProcessDataVcare server47 = new UploadAnodotProcessDataVcare();
			server47.setName("47");
			UploadAnodotProcessDataVcare server48 = new UploadAnodotProcessDataVcare();
			server48.setName("48");
			UploadAnodotProcessDataVcare server49 = new UploadAnodotProcessDataVcare();
			server49.setName("49");
			UploadAnodotProcessDataVcare server50 = new UploadAnodotProcessDataVcare();
			server50.setName("50");
			UploadAnodotProcessDataVcare server51 = new UploadAnodotProcessDataVcare();
			server51.setName("51");
			UploadAnodotProcessDataVcare server53 = new UploadAnodotProcessDataVcare();
			server53.setName("53");
			UploadAnodotProcessDataVcare server54 = new UploadAnodotProcessDataVcare();
			server54.setName("54");
			UploadAnodotProcessDataVcare server55 = new UploadAnodotProcessDataVcare();
			server55.setName("55");
			UploadAnodotProcessDataVcare server56 = new UploadAnodotProcessDataVcare();
			server56.setName("56");
			UploadAnodotProcessDataVcare server57 = new UploadAnodotProcessDataVcare();
			server57.setName("57");
			UploadAnodotProcessDataVcare server40 = new UploadAnodotProcessDataVcare();
			server40.setName("40");
			UploadAnodotProcessDataVcare server39 = new UploadAnodotProcessDataVcare();
			server39.setName("39");
			UploadAnodotProcessDataVcare server41 = new UploadAnodotProcessDataVcare();
			server41.setName("41");
			UploadAnodotProcessDataVcare server43 = new UploadAnodotProcessDataVcare();
			server43.setName("43");
			
			server42.start();
			server36.start();
			server37.start();
			server38.start();
			server47.start();
			server48.start();
			server49.start();
			server50.start();
			server51.start();
			server53.start();
			server54.start();
			server55.start();
			server56.start();
			server57.start();
			server40.start();
			server39.start();
			server41.start();
			server43.start();
	}
	
public void runThread(String ipAddress,int[] serverIds){
		
		try{
			long epoch = System.currentTimeMillis()/1000;
			String startTime = String.valueOf(epoch -180);
			String endTime = String.valueOf(epoch -180);
			uploadAnodotProcessData(ipAddress, startTime, endTime,serverIds);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void run() {
		try {
			while(true){
				String ipAddress = "10.1.105.67";
				// System.out.println("Runnable
				// Thread:"+Thread.currentThread().getName());
				int serverId = Integer.valueOf(Thread.currentThread().getName());
				runThread(ipAddress, new int[] { serverId });
				Thread.sleep(60000);
			}
		} catch (InterruptedException e) {

			System.out.println(e.getMessage());

		}

	}
	
	public static void uploadAnodotProcessData(String ipAddress,String startTime,String endTime,int[] serverIds) throws Exception{
		String userName ="rmadhira+api@kmbs.konicaminolta.us";
		String password ="Raki@123";
		for (int i = 0; i < serverIds.length; i++) {
			int serverId = serverIds[i];
			String jsonString = getDataFromAppFirst("https://"+ipAddress+"/api/servers/"+serverId+"/processes/?format=json&start="+startTime+"&end="+startTime,userName,password);
			if(!"NoDetailsFound".equalsIgnoreCase(jsonString)){
				uploadProcessData(serverId,jsonString,startTime,userName,password,ipAddress);
				System.out.println("ServerID  ProcessData::::::::::::::::::::::::: "+serverIds[i] +" :: Uploded ::: Time :::: "+startTime);
			}
		}
	}
	
	public static void uploadProcessData(int serverId,String ProcessIdData, String time,String userName, String password,String ipAddress) throws Exception{
		
		String processList[]= null;
		if (serverId == 42) {
			processList = new String[]{ "EIF Agent Concrete Service for Script","EIF Agent Concrete Service for Server", "EIF Agent Concrete Service" };
		} else if(serverId == 36){
			processList = new String[]{ "ServiceBody","sqlservr" };
		}else{
			processList = new String[]{ "KMCSBase","MSSQLSERVER","CSRC_ServerMonitor" };
		}

		JSONObject jsonObject = JSONObject.fromObject(ProcessIdData);
		JSONArray pdata = jsonObject.getJSONArray("data");
		String dataTime = "";
		int count = 0;
		for (int i = 0; i < pdata.size(); i++) {
			final JSONObject processIdJsonObj = pdata.getJSONObject(i);
			String processName= processIdJsonObj.getString("name");
			if(processName.indexOf(".")!= -1){
				processName = processName.substring(0, processName.indexOf("."));
			}	
			if (ArrayUtils.contains(processList, processName)) {
				String processDataJsonString = getDataFromAppFirst("https://" + ipAddress + "/api/processes/"
						+ processIdJsonObj.getString("uid") + "/data/?format=json&start=" + time + "&end=" + time,
						userName, password);
				jsonObject = JSONObject.fromObject(processDataJsonString);
				final JSONArray geodata = jsonObject.getJSONArray("data");
				JSONObject obj = new JSONObject();
				JSONArray jsonArray = getJsonArrayToUploadServerData(geodata, serverId,processName);
				obj.put("anodot", jsonArray);
				String aondotString = obj.getString("anodot");
				if (geodata.size() > 0) {
					sendRequestToAnodot(aondotString);
				}
				final JSONObject temp = geodata.getJSONObject(0);
				dataTime = temp.getString("time");
			}
			if ("ServiceNotification".equalsIgnoreCase(processName)) {
				count++;
			}

		}
		if (serverId == 42) {
			JSONArray jsonArray = new JSONArray();
			JSONObject tempJsonObject = new JSONObject();
			tempJsonObject.put("name", "server=" + getServerName(serverId)
					+ ".ProcessName=ServiceNotification.MetricType=NoOfProcesses.what=NoOfProcesses");
			tempJsonObject.put("timestamp", dataTime);
			tempJsonObject.put("value", count);
			jsonArray.add(tempJsonObject);
			sendRequestToAnodot(jsonArray.toString());
		}
	}
	
	public static JSONArray getJsonArrayToUploadServerData(final JSONArray AppfirstData,int serverId,String processName) {
		JSONArray jsonArray = new JSONArray();
		processName = processName.replaceAll(" ", "");
		for (int i = 0; i < AppfirstData.size(); i++) {
			final JSONObject data = AppfirstData.getJSONObject(i);
			String serverName =getServerName(serverId);
			addJsonObjectToArray("thread_num", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=thread_num.what=thread_num", jsonArray, data);
			addJsonObjectToArray("socket_num", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=socket_num.what=socket_num", jsonArray, data);
			addJsonObjectToArray("page_faults", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=page_faults.what=page_faults", jsonArray, data);
			addJsonObjectToArray("socket_write", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=socket_write.what=socket_write", jsonArray, data);
			addJsonObjectToArray("socket_read", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=socket_read.what=socket_read", jsonArray, data);
			addJsonObjectToArray("memory", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=memory.what=memory", jsonArray, data);
			addJsonObjectToArray("file_read", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=file_read.what=file_read", jsonArray, data);
			addJsonObjectToArray("registry_num", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=registry_num.what=registry_num", jsonArray, data);
			addJsonObjectToArray("critical_log", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=critical_log.what=critical_log", jsonArray, data);
			addJsonObjectToArray("file_write", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=file_write.what=file_write", jsonArray, data);
			addJsonObjectToArray("flags", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=flags.what=flags", jsonArray, data);
			addJsonObjectToArray("file_num", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=file_num.what=file_num", jsonArray, data);
			addJsonObjectToArray("response_num", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=response_num.what=response_num", jsonArray, data);
			addJsonObjectToArray("cpu", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=cpu.what=CPUutilization", jsonArray, data);
		}
		return jsonArray;
	}
	
	public static void addJsonObjectToArray(String metricType, String name, JSONArray jsonArray,
			final JSONObject data) {
		JSONObject tempJsonObject;
		tempJsonObject = new JSONObject();
		tempJsonObject.put("name", name);
		tempJsonObject.put("timestamp", data.getString("time"));
		try{
			tempJsonObject.put("value", data.getString(metricType));
		}catch(Exception e){
			//e.printStackTrace();
			tempJsonObject.put("value", "0");
		}
		jsonArray.add(tempJsonObject);
	}
	
	public static String   getDataFromAppFirst(String GET_URL,String userName,String password) throws Exception {

		String jsonString ="";
		
		disableSSLVerification();
		//System.out.println(":::::::::::::::::::::::::::::::::::: After  Certificate Verification ::::::::::::::::::::::::::::::::::::::::::::::::: ");
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        String encodedBytes = Base64.encodeBase64String((userName+":"+password).getBytes());
        con.setRequestMethod("GET");
        con.setRequestProperty("Authorization", "Basic "+encodedBytes);
		con.setRequestProperty("Content-Type", "application/json");
		//System.out.println("Authorization ::::: Basic "+encodedBytes);
        int responseCode = con.getResponseCode();
        //System.out.println("APPFIRST Response Code :: " + responseCode);
        if (responseCode == HttpsURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            // print result
            //System.out.println(response.toString());
            jsonString = response.toString();
        } else {
            System.out.println("GET request not worked");
            jsonString = "NoDetailsFound";
        }
        
        return jsonString;
	}
	
	public static void disableSSLVerification() {

        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };      
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);           
    }
	
public static void sendRequestToAnodot(String data)throws Exception{
		
		String GET_URL ="https://api.anodot.com/api/v1/metrics?token=56723b0ac97bc37f4a0ece0c";
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);
		String str =  data;
		//str =  "[{'name':'metric.example.1','timestamp':'1450830417','value':100,'tags':{'target_type':'counter'}},{'name':'metric.example.1','timestamp':'1450830441','value':200,'tags':{'target_type':'counter'}}]";
    	byte[] outputInBytes = str.getBytes("UTF-8");
    	OutputStream os = con.getOutputStream();
    	os.write( outputInBytes );    
    	os.close();
		int responseCode = con.getResponseCode();
        //System.out.println("Anodot Response Code :: " + responseCode);
	}

private static String getServerName(int serverId){
	String serverName ="";
	try {
		
		switch (serverId) {
			case 42:
					serverName ="vcareeif";
				break;
			case 36:
				serverName ="vcaredb";
			break;
			
			case 37:
				serverName ="vcarecom1";
			break;
			case 38:
				serverName ="vcarecom2";
			break;
			case 47:
				serverName ="vcarecom3";
			break;
			case 48:
				serverName ="vcarecom4";
			break;
			case 49:
				serverName ="vcarecom5";
			break;
			case 50:
				serverName ="vcarecom6";
			break;
			case 51:
				serverName ="vcarecom7";
			break;
			case 53:
				serverName ="vcarecom8";
			break;
			case 54:
				serverName ="vcarecom9";
			break;
			case 55:
				serverName ="vcarecom10";
			break;
			case 56:
				serverName ="vcarecom11";
			break;
			case 57:
				serverName ="vcarecom12";
			break;
			case 40:
				serverName ="vcarecom15";
			break;
			case 39:
				serverName ="vcarecom20";
			break;
			case 41:
				serverName ="vcarecom30";
			break;
			case 43:
				serverName ="vcarecom38";
			break;
		
		}
	} catch (Exception e) {
		// TODO: handle exception
	}
	return serverName;
	
}
}
