package com.appFirst.util;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class AnodotCSVForProcessData_1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
		System.setProperty("jsse.enableSNIExtension", "false");
		AnodotCSVForProcessData_1 http = new AnodotCSVForProcessData_1();
		String startTime = "1448956800"; // FROM dec 2015 1st 00:00:00 PDT 1448956800
		//String endTime ="1449475200"; //To Jan 20 2016 12:00:00 PDT 
		
		String endTime ="1453320000"; //To Jan 20 2016 12:00:00 PDT 
		System.out.println("endTime ::::::::::::::::::::::::::: "+endTime);
		int recordCount =1;
		int serverId= 42;
		String processName="EIF Agent Concrete Service for Script.exe";
		String processName1="EIF Agent Concrete Service for Server.exe";
		String processName2="EIF Agent Concrete Service.exe";
		
		String file = "C:\\Users\\rakesh\\Desktop\\anodot\\vcareeif\\Processes_DataFile_"+getServerName(serverId)+"_"+processName.substring(0, processName.indexOf("."))+"_"+startTime+"_"+endTime+"_4.csv";
		FileWriter fw = new FileWriter(file);
		String line = "TimeStamp,Value,MetricName\n";
		fw.write(line);
		
		String file1 = "C:\\Users\\rakesh\\Desktop\\anodot\\vcareeif\\Processes_DataFile_"+getServerName(serverId)+"_"+processName1.substring(0, processName1.indexOf("."))+"_"+startTime+"_"+endTime+"_4.csv";
		FileWriter fw1 = new FileWriter(file1);
		String line1 = "TimeStamp,Value,MetricName\n";
		fw1.write(line1);
		
		String file2 = "C:\\Users\\rakesh\\Desktop\\anodot\\vcareeif\\Processes_DataFile_"+getServerName(serverId)+"_"+processName2.substring(0, processName2.indexOf("."))+"_"+startTime+"_"+endTime+"_4.csv";
		FileWriter fw2 = new FileWriter(file2);
		String line2 = "TimeStamp,Value,MetricName\n";
		fw2.write(line2);
		
		startTime="1453185060";
		//endTime= "1449993600";
		
		// 10.1.105.67
		//frontend.konicaminoltabusiness.comcastbiz.net
		
		long start = System.currentTimeMillis();
		for(long k=Long.valueOf(startTime); k<=Long.valueOf(endTime);k= k+60){
			
			startTime = String.valueOf(k);
			
			//String jsonString = http.getServerData("https://frontend.konicaminoltabusiness.comcastbiz.net/api/servers/"+serverId+"/processes/?format=json&start="+startTime+"&end="+startTime,"rmadhira+api@kmbs.konicaminolta.us","Raki@123");
			String jsonString = http.getServerData("https://frontend.konicaminoltabusiness.comcastbiz.net/api/servers/"+serverId+"/processes/?format=json&start="+startTime+"&end="+startTime,"rmadhira+api@kmbs.konicaminolta.us","Raki@123");
			JSONObject jsonObject = JSONObject.fromObject(jsonString);
			JSONArray pdata = jsonObject.getJSONArray("data");
			for (int i = 0; i < pdata.size(); i++) {
				final JSONObject data = pdata.getJSONObject(i);
				try{
					if(processName.equalsIgnoreCase(data.getString("name"))){
						String processDataJsonString = http.getServerData("https://frontend.konicaminoltabusiness.comcastbiz.net/api/processes/"+data.getString("uid")+"/data/?format=json&start="+startTime+"&end="+startTime,"rmadhira+api@kmbs.konicaminolta.us","Raki@123");
						recordCount = wirteToCsvFle(processDataJsonString,serverId,fw,recordCount,processName.substring(0, processName.indexOf(".")));
					}else if(processName1.equalsIgnoreCase(data.getString("name"))){
						String processDataJsonString = http.getServerData("https://frontend.konicaminoltabusiness.comcastbiz.net/api/processes/"+data.getString("uid")+"/data/?format=json&start="+startTime+"&end="+startTime,"rmadhira+api@kmbs.konicaminolta.us","Raki@123");
						recordCount = wirteToCsvFle(processDataJsonString,serverId,fw1,recordCount,processName1.substring(0, processName1.indexOf(".")));
					}else if(processName2.equalsIgnoreCase(data.getString("name"))){
						String processDataJsonString = http.getServerData("https://frontend.konicaminoltabusiness.comcastbiz.net/api/processes/"+data.getString("uid")+"/data/?format=json&start="+startTime+"&end="+startTime,"rmadhira+api@kmbs.konicaminolta.us","Raki@123");
						recordCount = wirteToCsvFle(processDataJsonString,serverId,fw2,recordCount,processName2.substring(0, processName2.indexOf(".")));
					}
					
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			if(k == Long.valueOf(endTime)){
				System.out.println("End Time value ================================================= "+k);
			}
		}
		fw.close();
		fw1.close();
		fw2.close();
		System.out.println("recordCount ::::::::::::::::::: "+recordCount);
		long end = System.currentTimeMillis();
		System.out.println("Total Time to Create a File  :::::::::::::::::::::::::::::::::  "+((end - start) / 1000.0)+"sec");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static int wirteToCsvFle(String jsonString,int serverId,FileWriter fw, int recordCount,String processName) throws Exception {
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		JSONArray geodata = jsonObject.getJSONArray("data");
		processName = processName.replaceAll(" ", "");
		for (int i = 0; i < geodata.size(); i++) {
			final JSONObject data = geodata.getJSONObject(i);
			try{
				fw.write(getDataString("thread_num", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=thread_num.what=thread_num", data));
				fw.write(getDataString("socket_num", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=socket_num.what=socket_num", data));
				fw.write(getDataString("page_faults", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=page_faults.what=page_faults", data));
				fw.write(getDataString("socket_write", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=socket_write.what=socket_write", data));
				fw.write(getDataString("socket_read", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=socket_read.what=socket_read", data));
				fw.write(getDataString("memory", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=memory.what=memory", data));
				fw.write(getDataString("file_read", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=file_read.what=file_read", data));
				fw.write(getDataString("registry_num", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=registry_num.what=registry_num", data));
				fw.write(getDataString("critical_log", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=critical_log.what=critical_log", data));
				fw.write(getDataString("file_write", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=file_write.what=file_write", data));
				fw.write(getDataString("flags", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=flags.what=flags", data));
				fw.write(getDataString("file_num", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=file_num.what=file_num", data));
				fw.write(getDataString("response_num", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=response_num.what=response_num", data));
				fw.write(getDataString("cpu", "server="+getServerName(serverId)+".ProcessName="+processName+".MetricType=cpu.what=CPUutilization", data));
				
				recordCount += 14;
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return recordCount;
	}
	
	private static String getDataString(String metricType,String metricName,final JSONObject data){
		String dataString="";
		
		try {
			dataString = data.getString("time")+","+data.getString(metricType)+","+metricName+"\n";
		} catch (Exception e) {
			dataString = data.getString("time")+",0,"+metricName+"\n";
			// TODO: handle exception
		}
		return dataString;
		
	}
	
	private String  getServerData(String GET_URL,String userName,String password) throws Exception {

		String jsonString ="";
		
		disableSSLVerification();
		//System.out.println(":::::::::::::::::::::::::::::::::::: After  Certificate Verification ::::::::::::::::::::::::::::::::::::::::::::::::: ");
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        String encodedBytes = Base64.encodeBase64String((userName+":"+password).getBytes());
        con.setRequestMethod("GET");
        con.setRequestProperty("Authorization", "Basic "+encodedBytes);
		con.setRequestProperty("Content-Type", "application/json");
		//System.out.println("Authorization ::::: Basic "+encodedBytes);
        int responseCode = con.getResponseCode();
        System.out.println("APPFIRST Response Code :: " + responseCode);
        if (responseCode == HttpsURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            // print result
            //System.out.println(response.toString());
            jsonString = response.toString();
        } else {
            System.out.println("GET request not worked");
        }
        
        return jsonString;
	}
	
	public static void disableSSLVerification() {

        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };      
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);           
    }
	
	private static String getServerName(int serverId){
		String serverName ="";
		try {
			
			switch (serverId) {
				case 42:
						serverName ="vcareeif";
					break;
				case 36:
					serverName ="vcaredb";
				break;
				
				case 37:
					serverName ="vcarecom1";
				break;
				case 38:
					serverName ="vcarecom2";
				break;
				case 47:
					serverName ="vcarecom3";
				break;
				case 48:
					serverName ="vcarecom4";
				break;
				case 49:
					serverName ="vcarecom5";
				break;
				case 50:
					serverName ="vcarecom6";
				break;
				case 51:
					serverName ="vcarecom7";
				break;
				case 53:
					serverName ="vcarecom8";
				break;
				case 54:
					serverName ="vcarecom9";
				break;
				case 55:
					serverName ="vcarecom10";
				break;
				case 56:
					serverName ="vcarecom11";
				break;
				case 57:
					serverName ="vcarecom12";
				break;
				case 40:
					serverName ="vcarecom15";
				break;
				case 39:
					serverName ="vcarecom20";
				break;
				case 41:
					serverName ="vcarecom30";
				break;
				case 43:
					serverName ="vcarecom38";
				break;
			
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return serverName;
		
	}
}
