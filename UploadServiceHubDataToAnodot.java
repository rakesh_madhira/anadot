package com.appFirst.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class UploadServiceHubDataToAnodot  extends Thread{
	public static void main(String[] args) throws Exception {
		
		UploadServiceHubDataToAnodot server101 = new UploadServiceHubDataToAnodot();
		server101.setName("101");
		UploadServiceHubDataToAnodot server95 = new UploadServiceHubDataToAnodot();
		server95.setName("95");
		UploadServiceHubDataToAnodot server100 = new UploadServiceHubDataToAnodot();
		server100.setName("100");
		UploadServiceHubDataToAnodot server98 = new UploadServiceHubDataToAnodot();
		server98.setName("98");
		UploadServiceHubDataToAnodot server97 = new UploadServiceHubDataToAnodot();
		server97.setName("97");
		UploadServiceHubDataToAnodot server94 = new UploadServiceHubDataToAnodot();
		server94.setName("94");
		UploadServiceHubDataToAnodot server99 = new UploadServiceHubDataToAnodot();
		server99.setName("99");
		UploadServiceHubDataToAnodot server102 = new UploadServiceHubDataToAnodot();
		server102.setName("102");
		UploadServiceHubDataToAnodot server96 = new UploadServiceHubDataToAnodot();
		server96.setName("96");
		UploadServiceHubDataToAnodot server93 = new UploadServiceHubDataToAnodot();
		server93.setName("93");
		UploadServiceHubDataToAnodot server92 = new UploadServiceHubDataToAnodot();
		server92.setName("92");
		
		server101.start();
		server95.start();
		server100.start();
		server98.start();
		server97.start();
		server94.start();
		server99.start();
		server102.start();
		server96.start();
		server93.start();
		server92.start();
	}
public void runThread(String ipAddress,int[] serverIds){
		
		try{
			long epoch = System.currentTimeMillis()/1000;
			String startTime = String.valueOf(epoch -120);
			String endTime = String.valueOf(epoch -120);
			uploadAnodotServerData(ipAddress, startTime, endTime,serverIds);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	
	@Override
	public void run() {
		try {
			while(true){
				String ipAddress = "10.1.105.67";
				// System.out.println("Runnable
				// Thread:"+Thread.currentThread().getName());
				int serverId = Integer.valueOf(Thread.currentThread().getName());
				runThread(ipAddress, new int[] { serverId });
				Thread.sleep(60000);
			}
		} catch (InterruptedException e) {

			System.out.println(e.getMessage());

		}

	}
	
	public static void uploadAnodotServerData(String ipAddress,String startTime,String endTime,int[] serverIds) throws Exception{
		
		
		for (int i = 0; i < serverIds.length; i++) {
			int serverId = serverIds[i];
			//System.out.println("ServerID  ServerData ::::::::::::::::::::::::: "+serverIds[i]);
			//System.out.println("https://"+ipAddress+"/api/servers/"+serverId+"/data/?format=json&start="+startTime+"&end="+endTime);
			String jsonString = getDataFromAppFirst("https://"+ipAddress+"/api/servers/"+serverId+"/data/?format=json&start="+startTime+"&end="+endTime,"rmadhira+sh@kmbs.konicaminolta.us","Raki@123"); 
			if(!"NoDetailsFound".equalsIgnoreCase(jsonString)){
				UploadSeriveHubDataToAnodot.uploadServerDataToAnodot(jsonString,serverId);
				System.out.println("ServerID  Service Hub Data to Anodot:::::::::::: "+serverIds[i] +" :: Uploded ::: Time :::: "+startTime);
			}
		}
	}
	public static String   getDataFromAppFirst(String GET_URL,String userName,String password) throws Exception {

		String jsonString ="";
		
		disableSSLVerification();
		//System.out.println(":::::::::::::::::::::::::::::::::::: After  Certificate Verification ::::::::::::::::::::::::::::::::::::::::::::::::: ");
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        String encodedBytes = Base64.encodeBase64String((userName+":"+password).getBytes());
        con.setRequestMethod("GET");
        con.setRequestProperty("Authorization", "Basic "+encodedBytes);
		con.setRequestProperty("Content-Type", "application/json");
		//System.out.println("Authorization ::::: Basic "+encodedBytes);
        int responseCode = con.getResponseCode();
        //System.out.println("APPFIRST Response Code :: " + responseCode);
        if (responseCode == HttpsURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            // print result
            //System.out.println(response.toString());
            jsonString = response.toString();
        } else {
            System.out.println("GET request not worked :: "+GET_URL);
            jsonString = "NoDetailsFound";
        }
        
        return jsonString;
	}
	
	public static void disableSSLVerification() {

        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };      
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);           
    }
	
public static void sendRequestToAnodot(String data)throws Exception{
		
		String GET_URL ="https://api.anodot.com/api/v1/metrics?token=56f5570753e36acc3f32bb8e";
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);
		String str =  data;
		//str =  "[{'name':'metric.example.1','timestamp':'1450830417','value':100,'tags':{'target_type':'counter'}},{'name':'metric.example.1','timestamp':'1450830441','value':200,'tags':{'target_type':'counter'}}]";
    	byte[] outputInBytes = str.getBytes("UTF-8");
    	OutputStream os = con.getOutputStream();
    	os.write( outputInBytes );    
    	os.close();
		int responseCode = con.getResponseCode();
        //System.out.println("Anodot Response Code :: " + responseCode);
	}
}
