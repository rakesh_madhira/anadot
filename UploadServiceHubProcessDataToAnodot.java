package com.appFirst.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.ArrayUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class UploadServiceHubProcessDataToAnodot  extends Thread{
	public static void main(String[] args) throws Exception {
		
		UploadServiceHubProcessDataToAnodot server140 = new UploadServiceHubProcessDataToAnodot();
		server140.setName("140");
		UploadServiceHubProcessDataToAnodot server141 = new UploadServiceHubProcessDataToAnodot();
		server141.setName("141");
		UploadServiceHubProcessDataToAnodot server142 = new UploadServiceHubProcessDataToAnodot();
		server142.setName("142");
		UploadServiceHubProcessDataToAnodot server143 = new UploadServiceHubProcessDataToAnodot();
		server143.setName("143");
		UploadServiceHubProcessDataToAnodot server144 = new UploadServiceHubProcessDataToAnodot();
		server144.setName("144");
		UploadServiceHubProcessDataToAnodot server145 = new UploadServiceHubProcessDataToAnodot();
		server145.setName("145");
		UploadServiceHubProcessDataToAnodot server146 = new UploadServiceHubProcessDataToAnodot();
		server146.setName("146");
		
		server140.start();
		server141.start();
		server142.start();
		server143.start();
		server144.start();
		server145.start();
		server146.start();
		
	}
	
	public void runThread(String ipAddress,int[] serverIds){
		
		try{
			long epoch = System.currentTimeMillis()/1000;
			String startTime = String.valueOf(epoch -220);
			String endTime = String.valueOf(epoch -220);
			uploadServiceHubProcessDataToAnodot(ipAddress, startTime, endTime,serverIds);
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void run() {
		try {
			while(true){
				String ipAddress = "10.1.105.67";
				// System.out.println("Runnable
				// Thread:"+Thread.currentThread().getName());
				int processGroupID = Integer.valueOf(Thread.currentThread().getName());
				runThread(ipAddress, new int[] { processGroupID });
				Thread.sleep(60000);
			}
		} catch (InterruptedException e) {

			System.out.println(e.getMessage());

		}
	}
	
	public static void uploadServiceHubProcessDataToAnodot(String ipAddress,String startTime,String endTime,int[] processGroupIds) throws Exception{
		
		
		for (int i = 0; i < processGroupIds.length; i++) {
			int processGroupID = processGroupIds[i];
			//System.out.println("ServerID  ServerData ::::::::::::::::::::::::: "+serverIds[i]);
			//System.out.println("https://"+ipAddress+"/api/servers/"+serverId+"/data/?format=json&start="+startTime+"&end="+endTime);
			String jsonString = getDataFromAppFirst("https://"+ipAddress+"/api/applications/"+processGroupID+"/data/?format=json&start="+startTime+"&end="+endTime,"rmadhira+sh@kmbs.konicaminolta.us","Raki@123"); 
			if(!"NoDetailsFound".equalsIgnoreCase(jsonString)){
				uploadProcessGroupDataToAnodot(jsonString,processGroupID);
				System.out.println("ServerID  Application Data to Anodot:::::::::::: "+processGroupIds[i] +" :: Uploded ::: Time :::: "+startTime);
			}
		}
	}
	public static String   getDataFromAppFirst(String GET_URL,String userName,String password) throws Exception {

		String jsonString ="";
		
		disableSSLVerification();
		//System.out.println(":::::::::::::::::::::::::::::::::::: After  Certificate Verification ::::::::::::::::::::::::::::::::::::::::::::::::: ");
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        String encodedBytes = Base64.encodeBase64String((userName+":"+password).getBytes());
        con.setRequestMethod("GET");
        con.setRequestProperty("Authorization", "Basic "+encodedBytes);
		con.setRequestProperty("Content-Type", "application/json");
		//System.out.println("Authorization ::::: Basic "+encodedBytes);
        int responseCode = con.getResponseCode();
        //System.out.println("APPFIRST Response Code :: " + responseCode);
        if (responseCode == HttpsURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            // print result
            //System.out.println(response.toString());
            jsonString = response.toString();
        } else {
            System.out.println("GET request not worked :: "+GET_URL +"Username ::::: "+userName+" password :::::::::::: "+password);
            jsonString = "NoDetailsFound";
        }
        
        return jsonString;
	}
	
	public static void disableSSLVerification() {

        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };      
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);           
    }
	
public static void sendRequestToAnodot(String data)throws Exception{
		
		String GET_URL ="https://api.anodot.com/api/v1/metrics?token=56f5570753e36acc3f32bb8e";
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);
		String str =  data;
		//System.out.println(str);
		//str =  "[{'name':'metric.example.1','timestamp':'1450830417','value':100,'tags':{'target_type':'counter'}},{'name':'metric.example.1','timestamp':'1450830441','value':200,'tags':{'target_type':'counter'}}]";
    	byte[] outputInBytes = str.getBytes("UTF-8");
    	OutputStream os = con.getOutputStream();
    	os.write( outputInBytes );    
    	os.close();
		int responseCode = con.getResponseCode();
        //System.out.println("Anodot Response Code :: " + responseCode);
	}

	public static String uploadProcessGroupDataToAnodot(String jsonString,int prcessGroupID) throws Exception {
		String aondotString= "";
		String next = "";
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		final JSONObject pagination = jsonObject.getJSONObject("pagination"); 
		next = pagination.getString("next");
		final JSONArray geodata = jsonObject.getJSONArray("data");
		JSONObject obj = new JSONObject();
		JSONArray jsonArray = getJsonArrayToUploadServerData(geodata,prcessGroupID);
		obj.put("anodot", jsonArray);
		aondotString = obj.getString("anodot");
		if(geodata.size()>0){
			sendRequestToAnodot(aondotString);
		}
		return aondotString;
	}
	
	public static JSONArray getJsonArrayToUploadServerData(final JSONArray AppfirstData,int processGroupID) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < AppfirstData.size(); i++) {
			final JSONObject data = AppfirstData.getJSONObject(i);
			String processgroupName ="";
			
			switch (processGroupID) {
			case 140:
				processgroupName= "Apache";
				break;
			case 141:
					processgroupName= "Passenger";
					break;
			case 142:
					processgroupName= "PhusionPassenger";
					break;
			case 143:
					processgroupName= "Postgresql";
					break;
			case 144:
					processgroupName= "MongoDB";
					break;
			case 145:
					processgroupName= "Nginx";
					break;
			case 146:
					processgroupName= "Tomcat";
					break;
		}
			
			addJsonObjectToArray("thread_num", "processgroup="+processgroupName+".MetricType=thread_num.what=thread_num", jsonArray, data);
			addJsonObjectToArray("socket_num", "processgroup="+processgroupName+".MetricType=socket_num.what=socket_num", jsonArray, data);
			addJsonObjectToArray("page_faults", "processgroup="+processgroupName+".MetricType=page_faults.what=page_faults", jsonArray, data);
			addJsonObjectToArray("socket_write", "processgroup="+processgroupName+".MetricType=socket_write.what=socket_write", jsonArray, data);
			addJsonObjectToArray("socket_read", "processgroup="+processgroupName+".MetricType=socket_read.what=socket_read", jsonArray, data);
			addJsonObjectToArray("memory", "processgroup="+processgroupName+".MetricType=memory.what=memory", jsonArray, data);
			addJsonObjectToArray("file_read", "processgroup="+processgroupName+".MetricType=file_read.what=file_read", jsonArray, data);
			addJsonObjectToArray("registry_num", "processgroup="+processgroupName+".MetricType=registry_num.what=registry_num", jsonArray, data);
			addJsonObjectToArray("critical_log", "processgroup="+processgroupName+".MetricType=critical_log.what=critical_log", jsonArray, data);
			addJsonObjectToArray("file_write", "processgroup="+processgroupName+".MetricType=file_write.what=file_write", jsonArray, data);
			addJsonObjectToArray("flags", "processgroup="+processgroupName+".MetricType=flags.what=flags", jsonArray, data);
			addJsonObjectToArray("file_num", "processgroup="+processgroupName+".MetricType=file_num.what=file_num", jsonArray, data);
			addJsonObjectToArray("response_num", "processgroup="+processgroupName+".MetricType=response_num.what=response_num", jsonArray, data);
			addJsonObjectToArray("cpu", "processgroup="+processgroupName+".MetricType=cpu.what=CPUutilization", jsonArray, data);
			
			
		}
		return jsonArray;
	}
	public static void addJsonObjectToArray(String metricType, String name, JSONArray jsonArray,
			final JSONObject data) {
		JSONObject tempJsonObject;
		tempJsonObject = new JSONObject();
		tempJsonObject.put("name", name);
		tempJsonObject.put("timestamp", data.getString("time"));
		try{
			tempJsonObject.put("value", data.getString(metricType));
		}catch(Exception e){
			//e.printStackTrace();
			tempJsonObject.put("value", "0");
		}
		jsonArray.add(tempJsonObject);
	}
	
	
}
