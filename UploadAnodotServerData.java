package com.appFirst.util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class UploadAnodotServerData extends UploadAnodotData {

	public static String uploadServerDataToAnodot(String jsonString,int serverId) throws Exception {
		String aondotString= "";
		String next = "";
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		final JSONObject pagination = jsonObject.getJSONObject("pagination"); 
		next = pagination.getString("next");
		final JSONArray geodata = jsonObject.getJSONArray("data");
		JSONObject obj = new JSONObject();
		JSONArray jsonArray = getJsonArrayToUploadServerData(geodata,serverId);
		obj.put("anodot", jsonArray);
		aondotString = obj.getString("anodot");
		if(geodata.size()>0){
			sendRequestToAnodot(aondotString);
		}
		return aondotString;
	}
	
	public static JSONArray getJsonArrayToUploadServerData(final JSONArray AppfirstData,int serverId) {
		JSONArray jsonArray = new JSONArray();
		for (int i = 0; i < AppfirstData.size(); i++) {
			final JSONObject data = AppfirstData.getJSONObject(i);
			String serverName ="";
			
			switch (serverId) {
			case 52:
				serverName= "brightstor1";
				break;
		case 72:
				serverName= "brightstor2";
				break;
		case 73:
				serverName= "crmcp1a01";
				break;
		case 74:
				serverName= "crmcp1a02";
				break;
		case 75:
				serverName= "crmcp1a03";
				break;
		case 76:
				serverName= "crmcp1a04";
				break;
		case 77:
				serverName= "crmcp1a05";
				break;
		case 78:
				serverName= "crmcp1a06";
				break;
		case 79:
				serverName= "crmcp1a07";
				break;
		case 80:
				serverName= "crmcp1a08";
				break;
		case 81:
				serverName= "crmcp1a09";
				break;
		case 82:
				serverName= "crmcp1a10";
				break;
		case 25:
				serverName= "jetformprod";
				break;
		case 63:
				serverName= "jetformtest";
				break;
		case 17:
				serverName= "kmbop1";
				break;
		case 18:
				serverName= "kmbop2";
				break;
		case 19:
				serverName= "kmbop3";
				break;
		case 20:
				serverName= "kmbop4";
				break;
		case 21:
				serverName= "kmbop5";
				break;
		case 22:
				serverName= "kmbop6";
				break;
		case 70:
				serverName= "kmbsdssrdg1";
				break;
		case 23:
				serverName= "kmcmsdbprd";
				break;
		case 30:
				serverName= "kmwebbop1";
				break;
		case 31:
				serverName= "kmwebbop2";
				break;
		case 24:
				serverName= "pxinvoiceapp";
				break;
		case 58:
				serverName= "sappbw";
				break;
		case 59:
				serverName= "sappbwapp1";
				break;
		case 60:
				serverName= "sappbwapp2";
				break;
		case 61:
				serverName= "sappbwapp3";
				break;
		case 62:
				serverName= "sappbwapp4";
				break;
		case 29:
				serverName= "sapprd";
				break;
		case 27:
				serverName= "sapprda18";
				break;
		case 28:
				serverName= "sapprda19";
				break;
		case 37:
				serverName= "vcarecom1";
				break;
		case 55:
				serverName= "vcarecom10";
				break;
		case 56:
				serverName= "vcarecom11";
				break;
		case 57:
				serverName= "vcarecom12";
				break;
		case 40:
				serverName= "vcarecom15";
				break;
		case 38:
				serverName= "vcarecom2";
				break;
		case 39:
				serverName= "vcarecom20";
				break;
		case 47:
				serverName= "vcarecom3";
				break;
		case 41:
				serverName= "vcarecom30";
				break;
		case 43:
				serverName= "vcarecom38";
				break;
		case 48:
				serverName= "vcarecom4";
				break;
		case 49:
				serverName= "vcarecom5";
				break;
		case 50:
				serverName= "vcarecom6";
				break;
		case 51:
				serverName= "vcarecom7";
				break;
		case 53:
				serverName= "vcarecom8";
				break;
		case 54:
				serverName= "vcarecom9";
				break;
		case 36:
				serverName= "vcaredb";
				break;
		case 34:
				serverName= "vcaredbtst";
				break;
		case 42:
				serverName= "vcareeif";
				break;
		case 71:
				serverName= "vcareweb1";
				break;
		case 108:
				serverName= "kmbop6a";
				break;
		case 109:
			serverName= "kmbop5a";
			break;
		case 110:
			serverName= "kmbop4a";
			break;
		case 111:
			serverName= "kmbop3a";
			break;
		case 112:
			serverName= "kmbop2a";
			break;
			
			}
			
			addJsonObjectToArray("cpu", "server="+serverName+".MetricType=cpu.what=CPUutilization", jsonArray, data);
			addJsonObjectToArray("thread_num","server="+serverName+".MetricType=thread.what=ThreadNum", jsonArray, data);
			addJsonObjectToArray("memory","server="+serverName+".MetricType=memory.what=memory", jsonArray, data);
			addJsonObjectToArray("process_num","server="+serverName+".MetricType=process_num.what=process_num", jsonArray, data);
			addJsonObjectToArray("page_faults","server="+serverName+".MetricType=page_faults.what=page_faults", jsonArray, data);
			addJsonObjectToArray("disk_busy_max","server="+serverName+".MetricType=disk_busy_max.what=disk_busy_max", jsonArray, data);
			addJsonObjectToArray("diskp_max","server="+serverName+".MetricType=diskusage_max.what=diskusage_max", jsonArray, data);
			addJsonObjectToArray("avg_response_time","server="+serverName+".MetricType=avg_response_time.what=avg_response_time", jsonArray, data);
			addJsonObjectToArray("socket_write","server="+serverName+".MetricType=InboundNetworkTraffic.what=InboundNetworkTraffic", jsonArray, data);
			addJsonObjectToArray("srw_max","server="+serverName+".MetricType=OutboundNetworkTraffic.what=OutboundNetworkTraffic", jsonArray, data);
			
			
		}
		return jsonArray;
	}
	
	public static void addJsonObjectToArray(String metricType, String name, JSONArray jsonArray,
			final JSONObject data) {
		JSONObject tempJsonObject;
		tempJsonObject = new JSONObject();
		tempJsonObject.put("name", name);
		tempJsonObject.put("timestamp", data.getString("time"));
		try{
			tempJsonObject.put("value", data.getString(metricType));
		}catch(Exception e){
			//e.printStackTrace();
			tempJsonObject.put("value", "0");
		}
		jsonArray.add(tempJsonObject);
	}

	
	
	
}
