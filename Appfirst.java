package com.appFirst.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class Appfirst {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{
		System.setProperty("jsse.enableSNIExtension", "false");
		Appfirst http = new Appfirst();
		System.out.println("Testing 1 - Send Http GET request");
		//http.getServerData("https://frontend.konicaminoltabusiness.comcastbiz.net/api/servers/52/?format=json","rmadhira+api@kmbs.konicaminolta.us","1608433977");
		//http.getServerData("https://frontend.konicaminoltabusiness.comcastbiz.net/api/servers/52/?format=json","rmadhira+api@kmbs.konicaminolta.us","Raki@123");
		//String jsonString  =  http.getServerData("https://frontend.konicaminoltabusiness.comcastbiz.net/api/servers/42/data/?format=json&num=60","rmadhira+api@kmbs.konicaminolta.us","Raki@123");
		//String jsonString  =  http.getServerData("https://10.1.105.67/api/servers/42/data/?format=json&start=1448697600&end=1451116800","rmadhira+api@kmbs.konicaminolta.us","Raki@123");
		//String jsonString  =  http.getServerData("https://10.1.105.67/api/servers/42/data/?format=json&start=1448697600&end=1451336784","rmadhira+api@kmbs.konicaminolta.us","Raki@123");
		//String jsonString  =  http.getServerData("https://10.1.105.67/api/servers/42/data/?format=json&start=1443682800&end=1451338853","rmadhira+api@kmbs.konicaminolta.us","Raki@123");
		//String jsonString  =  http.getServerData("https://10.1.105.67/api/servers/42/data/?format=json&num=1","rmadhira+api@kmbs.konicaminolta.us","Raki@123");
		//String cpudata = getAnodotJsonString(jsonString,"cpu","sever.vcareeif.MetricType=cpu.what=cpuUtilization");
		//String threadData = getAnodotJsonString(jsonString,"thread_num","sever.vcareeif.MetricType=thread.what=ThreadNum");
		//String memorydata = getAnodotJsonString(jsonString,"memory","sever.vcareeif.MetricType=memory.what=memory");
		//http.sendRequestToAnodot("",data);
		/*String stratTime = "1446015600"; 
		String endTime ="1451504694"; */
		
		String stratTime = "1446015600"; 
		String endTime ="1451931468"; 
		
		String jsonString  =  http.getServerData("https://10.1.105.67/api/servers/42/data/?format=json&start="+stratTime+"&end="+endTime,"rmadhira+api@kmbs.konicaminolta.us","Raki@123"); // vcareeif
		
		/*getAnodotJsonString(jsonString,"cpu","server=vcareeif.MetricType=cpu.what=CPUutilization");
		System.out.println("CPUutilization  is completed");
		getAnodotJsonString(jsonString,"thread_num","server=vcareeif.MetricType=thread.what=ThreadNum");
		System.out.println("ThreadNum  is completed");
		getAnodotJsonString(jsonString,"memory","server=vcareeif.MetricType=memory.what=memory");
		System.out.println("memory  is completed");
		getAnodotJsonString(jsonString,"process_num","server=vcareeif.MetricType=process_num.what=process_num");
		System.out.println("process_num  is completed");
		getAnodotJsonString(jsonString,"page_faults","server=vcareeif.MetricType=page_faults.what=page_faults");
		System.out.println("page_faults  is completed");
		getAnodotJsonString(jsonString,"disk_busy_max","server=vcareeif.MetricType=disk_busy_max.what=disk_busy_max");
		System.out.println("disk_busy_max  is completed");
		getAnodotJsonString(jsonString,"diskp_max","server=vcareeif.MetricType=diskusage_max.what=diskusage_max");
		System.out.println("diskusage_max  is completed");
		getAnodotJsonString(jsonString,"avg_response_time","server=vcareeif.MetricType=avg_response_time.what=avg_response_time");
		System.out.println("avg_response_time  is completed");
		getAnodotJsonString(jsonString,"socket_write","server=vcareeif.MetricType=InboundNetworkTraffic.what=InboundNetworkTraffic");
		System.out.println("InboundNetworkTraffic  is completed");
		getAnodotJsonString(jsonString,"srw_max","server=vcareeif.MetricType=OutboundNetworkTraffic.what=OutboundNetworkTraffic");
		System.out.println("OutboundNetworkTraffic  is completed");*/
		
		jsonString  =  http.getServerData("https://10.1.105.67/api/servers/36/data/?format=json&start="+stratTime+"&end="+endTime,"rmadhira+api@kmbs.konicaminolta.us","Raki@123"); //vcaredb
		
		/*getAnodotJsonString(jsonString,"cpu","server=vcaredb.MetricType=cpu.what=CPUutilization");
		System.out.println("CPUutilization  is completed");*/
		getAnodotJsonString(jsonString,"thread_num","server=vcaredb.MetricType=thread.what=ThreadNum");
		System.out.println("ThreadNum  is completed");
		getAnodotJsonString(jsonString,"memory","server=vcaredb.MetricType=memory.what=memory");
		System.out.println("memory  is completed");
		getAnodotJsonString(jsonString,"process_num","server=vcaredb.MetricType=process_num.what=process_num");
		System.out.println("process_num  is completed");
		getAnodotJsonString(jsonString,"page_faults","server=vcaredb.MetricType=page_faults.what=page_faults");
		System.out.println("page_faults  is completed");
		getAnodotJsonString(jsonString,"disk_busy_max","server=vcaredb.MetricType=disk_busy_max.what=disk_busy_max");
		System.out.println("disk_busy_max  is completed");
		getAnodotJsonString(jsonString,"diskp_max","server=vcaredb.MetricType=diskusage_max.what=diskusage_max");
		System.out.println("diskusage_max  is completed");
		getAnodotJsonString(jsonString,"avg_response_time","server=vcaredb.MetricType=avg_response_time.what=avg_response_time");
		System.out.println("avg_response_time  is completed");
		getAnodotJsonString(jsonString,"socket_write","server=vcaredb.MetricType=InboundNetworkTraffic.what=InboundNetworkTraffic");
		System.out.println("InboundNetworkTraffic  is completed");
		getAnodotJsonString(jsonString,"srw_max","server=vcaredb.MetricType=OutboundNetworkTraffic.what=OutboundNetworkTraffic");
		System.out.println("OutboundNetworkTraffic  is completed");
		
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

	public static String getAnodotJsonString(String jsonString,String metricType,String name) throws Exception {
		String aondotString= "";
		String next = "";
		//System.out.println("Json String :::::::::::::::::::: "+jsonString);
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		final JSONObject pagination = jsonObject.getJSONObject("pagination"); 
		next = pagination.getString("next");
		final JSONArray geodata = jsonObject.getJSONArray("data");
		JSONObject obj = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		JSONObject tempJsonObject = null;
		System.out.println("Data Array Size :::  "+geodata.size());
		for (int i = 0; i < geodata.size(); i++) {
			final JSONObject data = geodata.getJSONObject(i);
			//System.out.println("Time :::: "+data.getString("time"));
			//System.out.println("CPU ::::: "+data.getString("cpu"));

			tempJsonObject = new JSONObject();
			tempJsonObject.put("name", name);
			tempJsonObject.put("timestamp", data.getString("time"));
			//tempJsonObject.put("value", data.getString("cpu"));
			try{
				tempJsonObject.put("value", data.getString(metricType));
			}catch(Exception e){
				e.printStackTrace();
				tempJsonObject.put("value", "0");
			}
			jsonArray.add(tempJsonObject);
		}
		jsonObject.put("anodot", jsonArray);
		aondotString = jsonObject.getString("anodot");
		//System.out.println("aondotString ::::::: "+aondotString);
		if(geodata.size()>0){
			//sendRequestToAnodot("", aondotString);
		}
		if(next!=null && !"null".equalsIgnoreCase(next)){
			
			while(next!=null && !"null".equalsIgnoreCase(next)){
				String tempString  =  new Appfirst().getServerData(next,"rmadhira+api@kmbs.konicaminolta.us","Raki@123");	
				JSONObject nextObject = JSONObject.fromObject(tempString);
				final JSONObject nextPagination = nextObject.getJSONObject("pagination"); 
				next = nextPagination.getString("next");
				final JSONArray nextpagedata = nextObject.getJSONArray("data");
				JSONObject obj1 = new JSONObject();
				JSONArray jsonArray1 = new JSONArray();
				System.out.println(nextpagedata.size());
				for (int i = 0; i < nextpagedata.size(); i++) {
					final JSONObject data = nextpagedata.getJSONObject(i);
					//System.out.println("Time :::: "+data.getString("time"));
					//System.out.println("CPU ::::: "+data.getString("cpu"));

					tempJsonObject = new JSONObject();
					tempJsonObject.put("name", name);
					tempJsonObject.put("timestamp", data.getString("time"));
					try{
						tempJsonObject.put("value", data.getString(metricType));
					}catch(Exception e){
						e.printStackTrace();
						tempJsonObject.put("value", "0");
					}
					jsonArray.add(tempJsonObject);
				}
				nextObject.put("anodot", jsonArray);
				aondotString = nextObject.getString("anodot");
				//System.out.println("aondotString ::::::: "+aondotString);
				if(nextpagedata.size()>0){
					//sendRequestToAnodot("", aondotString);	
				}
				System.out.println("next Pagination  ::::::: "+next);
			}
			
			
		}
		
		return aondotString;
	}
	
	
	public static String getAnodotJsonStringTest(String jsonString,String metricType,String name) throws Exception {
		String aondotString= "";
		String next = "";
		//System.out.println("Json String :::::::::::::::::::: "+jsonString);
		JSONObject jsonObject = JSONObject.fromObject(jsonString);
		final JSONObject pagination = jsonObject.getJSONObject("pagination"); 
		next = pagination.getString("next");
		final JSONArray geodata = jsonObject.getJSONArray("data");
		JSONObject obj = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		JSONObject tempJsonObject = null;
		System.out.println("Data Array Size :::  "+geodata.size());
		for (int i = 0; i < geodata.size(); i++) {
			final JSONObject data = geodata.getJSONObject(i);
			//System.out.println("Time :::: "+data.getString("time"));
			//System.out.println("CPU ::::: "+data.getString("cpu"));

			tempJsonObject = new JSONObject();
			tempJsonObject.put("name", name);
			tempJsonObject.put("timestamp", data.getString("time"));
			//tempJsonObject.put("value", data.getString("cpu"));
			tempJsonObject.put("value", data.getString(metricType));
			jsonArray.add(tempJsonObject);
		}
		jsonObject.put("anodot", jsonArray);
		aondotString = jsonObject.getString("anodot");
		System.out.println("aondotString ::::::: "+aondotString);
		if(next!=null && !"null".equalsIgnoreCase(next)){
			System.out.println("Next in If ::::::::::: "+next);
			
			while(next!=null && !"null".equalsIgnoreCase(next)){
				String tempString  =  new Appfirst().getServerData(next,"rmadhira+api@kmbs.konicaminolta.us","Raki@123");	
				JSONObject nextObject = JSONObject.fromObject(tempString);
				final JSONObject nextPagination = nextObject.getJSONObject("pagination"); 
				next = nextPagination.getString("next");
				final JSONArray nextpagedata = nextObject.getJSONArray("data");
				JSONObject obj1 = new JSONObject();
				JSONArray jsonArray1 = new JSONArray();
				System.out.println(nextpagedata.size());
				for (int i = 0; i < nextpagedata.size(); i++) {
					final JSONObject data = nextpagedata.getJSONObject(i);
					//System.out.println("Time :::: "+data.getString("time"));
					//System.out.println("CPU ::::: "+data.getString("cpu"));

					tempJsonObject = new JSONObject();
					tempJsonObject.put("name", name);
					tempJsonObject.put("timestamp", data.getString("time"));
					try{
						tempJsonObject.put("value", data.getString(metricType));
					}catch(Exception e){
						e.printStackTrace();
						tempJsonObject.put("value", "0");
					}
					jsonArray.add(tempJsonObject);
				}
				nextObject.put("anodot", jsonArray);
				aondotString = nextObject.getString("anodot");
				System.out.println("aondotString ::::::: "+aondotString);
				System.out.println("next Pagination  ::::::: "+next);
			}
			
			
		}
		return aondotString;
	}
	
	private static void sendRequestToAnodot(String GET_URL,String data)throws Exception{
		
		GET_URL ="https://api.anodot.com/api/v1/metrics?token=56723b0ac97bc37f4a0ece0c";
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);
		String str =  data;
		//str =  "[{'name':'metric.example.1','timestamp':'1450830417','value':100,'tags':{'target_type':'counter'}},{'name':'metric.example.1','timestamp':'1450830441','value':200,'tags':{'target_type':'counter'}}]";
    	byte[] outputInBytes = str.getBytes("UTF-8");
    	OutputStream os = con.getOutputStream();
    	os.write( outputInBytes );    
    	os.close();
		int responseCode = con.getResponseCode();
        System.out.println("Anodot Response Code :: " + responseCode);
	}
	
	private String  getServerData(String GET_URL,String userName,String password) throws Exception {

		String jsonString ="";
		
		disableSSLVerification();
		//System.out.println(":::::::::::::::::::::::::::::::::::: After  Certificate Verification ::::::::::::::::::::::::::::::::::::::::::::::::: ");
		URL obj = new URL(GET_URL);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        String encodedBytes = Base64.encodeBase64String((userName+":"+password).getBytes());
        con.setRequestMethod("GET");
        con.setRequestProperty("Authorization", "Basic "+encodedBytes);
		con.setRequestProperty("Content-Type", "application/json");
		//System.out.println("Authorization ::::: Basic "+encodedBytes);
        int responseCode = con.getResponseCode();
        System.out.println("APPFIRST Response Code :: " + responseCode);
        if (responseCode == HttpsURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            // print result
            //System.out.println(response.toString());
            jsonString = response.toString();
        } else {
            System.out.println("GET request not worked");
        }
        
        return jsonString;
	}
	
	public static void disableSSLVerification() {

        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };

        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };      
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);           
    }
}
